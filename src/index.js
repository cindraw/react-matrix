import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import utils from './modules/utils'
import getChars from './modules/chars'

const
	amount = 50 ,
	gridspace = 20,
	initMatrix = amount => {
		let
			array = getChars(amount),
			data = []

		for (let i = 0; i < array.length; i++) {

			data.push({
				char: array[i],
				pos: {
					x: utils.getXPosition(gridspace),
					y: utils.getYPosition(false),
					get end() { return utils.getEndYPosition(this.y) }
				}
			})

		}
		return data
	}


class Characters extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			value: props.value,
			id: props.id,
			top: props.top,
			left: props.left,
			class: 'letter-div',
			el: undefined,
			height: undefined,
			zIndex: props.id,
			move: undefined,
			time: { max: 30, min: 10 }
		}
	}

	componentDidMount() {
		// setTimeout(() => this.setState({ top: this.props.endposition }), Math.random() * 10000)
		let el = document.getElementById(this.state.id)
		this.setState({
			top: this.props.endposition,
			el: el,
			height: el.getBoundingClientRect().height,
			move: this.randomTime(),
		})
		this.checkYPos()
	}

	checkYPos() {
		setInterval(() => {
			if (this.state.el.getBoundingClientRect().top >= this.props.endposition - 10) {
				this.returnToOrigin()
				setTimeout(() => this.continueFalling(), 0)
			}
		}, 33)

	}

	randomTime() {
		let time = Math.floor(Math.random() * this.state.time.max) + this.state.time.min,
			move = `all ${time}s ease-in`
		return move
	}

	onMouseOver() {
		this.changeCharacters()
	}

	changeCharacters() {
		this.setState({
			value: getChars(1)[0]
		})
	}

	continueFalling() {
		this.setState({
			top: this.props.endposition,
			move: this.randomTime(),
		})
	}

	returnToOrigin() {
		this.changeCharacters()
		this.setState({
			top: -300 - this.state.height,
			left: utils.getXPosition(gridspace),
			move: 'all 0s',
		})
	}

	render() {
		return (
			<div
				className={this.state.class}
				onMouseOver={() => this.onMouseOver()}
				style={{ top: this.state.top + 'px', left: this.state.left + 'px', transition: this.state.move, zIndex: this.state.zIndex }}
				id={this.state.id}
			>
				{this.state.value}
			</div >
		)
	}
}


class LetterGenerator extends React.Component {
	render() {
		return (
			<div>
				{initMatrix(amount).map((value, key) => (
					<Characters
						top={value.pos.y}
						left={value.pos.x}
						endposition={value.pos.end}
						key={key}
						value={value.char}
						id={key}
					/>
				))}
			</div>
		)
	}
}

class Holder extends React.Component {
	render() {
		return (
			<div>
				<h1>Matrix Generator</h1>
				<LetterGenerator />
			</div>);
	}
}

ReactDOM.render(
	<Holder />,
	document.getElementById('root')
);



