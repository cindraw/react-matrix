module.exports = {

	shuffle: (array) => { // https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array#6274398
		let counter = array.length;
		// While there are elements in the array
		while (counter > 0) {
			// Pick a random index
			let index = Math.floor(Math.random() * counter);
			// Decrease counter by 1
			counter--;
			// And swap the last element with it
			let temp = array[counter];
			array[counter] = array[index];
			array[index] = temp;
		}
		return array;
	},

	getXPosition: (ceiling) => {
		//get a point in the width of the window
		let
			xposition = Math.trunc(Math.random() * window.innerWidth)
		// lock everything on a grid using ceiling
		xposition = (Math.ceil(xposition / ceiling) * ceiling) - ceiling
		return xposition
	},

	getYPosition: fill => {
		// where character animation starts
		if (fill) { return Math.trunc(Math.random() * window.innerHeight * 2) - window.innerHeight }
		else { return Math.trunc(Math.random() * window.innerHeight) - window.innerHeight * 1.5 }
	},

	getEndYPosition: () => {
		// where character animation ends
		// return Math.trunc(Math.random() * 1000) + window.innerHeight

		return window.innerHeight
	}
}