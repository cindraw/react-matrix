const
	// https://www.lexilogos.com/keyboard/katakana.htm
	chars = `アァタナハハマヤャラワガザダバパイィキシチニヒミリヰギジヂビピプブヅズグルュユムフヌツスクゥウエェケセテネヘメレヱゲゼデベペポボドゾゴヲロョヨモホノトソコォオヴッン・ーヽヾ、。'`,
	// unused = `ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&&*(){}[]?><,./;':"-=_+`,

	random = x => {
		return Math.floor(Math.random() * x)
	},

	getString = () => {
		// getString strings
		let string = '',
			maxStringLength = 50,
			minimumLength = 10

		for (let i = 0; i < random(maxStringLength - minimumLength) + minimumLength; i++) {
			string += chars.charAt(random(chars.length))
		}
		return string
	},

	getArrays = x => {
		let array = []
		for (let i = 0; i < x; i++) {
			array.push(getString())
		}
		return array
	}


module.exports = x => getArrays(x)